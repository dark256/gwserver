const EventEmitter = require('events');

class Room extends EventEmitter {

  constructor(id) {
    super();
    this.id = id;
    this.clients = {};
    this.log = [];
    this.clientCount = 0;
  }

  addClient(client) {

    this.clients[client.id] = client;
    this.clientCount += 1;

    const onClientMessageHandler = this.onClientMessageHandler.bind(this);
    const onClientLeftHandler = this.onClientLeftHandler.bind(this);

    client.addListener('message', onClientMessageHandler);
    client.addListener('left', onClientLeftHandler);
    client.send({ event: 'login', id: client.id, name: client.name, room: this.id });

    const users = {};
    for (const id in this.clients) {
      if (this.clients.hasOwnProperty(id)) {
        users[id] = this.clients[id].name;
      }
    }

    client.send({ event: 'users', users });

    this.broadcast({ event: 'joined', id: client.id, name: client.name });
  }

  removeClient(id) {
    if (!this.clients[id]) {
      return
    }
    this.onClientLeftHandler(id);
  }

  onClientMessageHandler(data) {
    this.broadcast({ event: 'message', id: data.id, name: data.name, message: data.message });
  }

  onClientLeftHandler(data) {
    this.clients[data.id].destroy();
    delete this.clients[data.id];
    this.clientCount -= 1;
    this.broadcast({ event: 'left', id: data.id, name: data.name });
    this.emit('clientLeft');
  }

  broadcast(data) {
    this.log.push(data);
    for (const id in this.clients) {
      if (this.clients.hasOwnProperty(id)) {
        this.clients[id].send(data);
      }
    }
  }

  destroy() {
    for (const id in this.clients) {
      if (this.clients.hasOwnProperty(id)) {
        this.removeClient(id);
      }
    }
    this.id = null;
    this.clients = null;
    this.log = null;
  }
}

module.exports = Room;
