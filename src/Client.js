const Socket = require('net');
const EventEmitter = require('events');

class Client extends EventEmitter {

  constructor(socket, id, name) {
    super();
    this.id = id;
    this.name = name;
    this._socket = socket;
    this._socket.on('message', (data) => this.onMessageHandler(data));
    this._socket.on('disconnect', () => this.onDisconnectHandler());
  }

  send(data) {
    this._socket.emit('event', data);
  }

  onMessageHandler(data) {
    this.emit('message', { id: this.id, name: this.name, message: data.message });
  }

  onDisconnectHandler() {
    this.emit('left', { id: this.id, name: this.name });
  }

  destroy() {
    this._socket.removeAllListeners();
    this._socket = null;
  }
}

module.exports = Client;
