const Server = require('./Server');
const Room = require('./Room');

const rooms = {};
const server = new Server('localhost', 8081);

server.on('clientJoined', data => {
  room = openRoom(data.room);
  room.addClient(data.client);
});

function openRoom(id) {
  id = id || process.hrtime.bigint().toString();
  if (!rooms[id]) {
    let room = rooms[id] = new Room(id);
    room.addListener('clientLeft', () => closeRoom(room));
  }
  return rooms[id];
}

function closeRoom(room) {
  if (room.clientCount > 0) {
    return;
  }

  delete rooms[room.id];
  room.destroy();
}
