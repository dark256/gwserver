const server = require('http').createServer();
const io = require('socket.io')(server, { transports: ['websocket', 'polling'] });
const EventEmitter = require('events');

const Client = require('./Client');

class Server extends EventEmitter {

  constructor(host = 'localhost', port = 8081) {
    super();
    server.listen(port, host, () => {
      console.log(`server started on ${host}:${port}`);
      io.on('connection', (socket) => {
        console.log(`new connection from ${socket.client.conn.remoteAddress}`);
        socket.once('login', (data) => {
          this.emit('clientJoined', { client: new Client(socket, socket.client.id, data.name), room: data.room });
        });
      });
    });
  }
}

module.exports = Server;
